module.exports = function(config) {
  config.set({
    basePath: "",
    frameworks: ["mocha"],
    webpack: {
      mode: "development",
      module: {
        rules: [
          {
            test: /\.js?$/,
            loader: "babel-loader",
            options: {
              presets: ["@babel/env"]
            },
            exclude: ["/node_modules"]
          }
        ]
      }
    },
    files: [{ pattern: "src/**/test/**/*.spec.js", watched: true }],
    preprocessors: {
      "src/**/test/**/*.spec.js": ["webpack"]
    },
    coverageReporter: {
      type: "text-summary"
    },
    exclude: [],
    reporters: ["spec", "coverage"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["ChromeHeadless"],
    singleRun: false,
    concurrency: Infinity
  });
};
