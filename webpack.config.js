const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: "development",
  entry: "./src/app.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js"
  },
  devServer: {
    index: "index.html",
    port: 9000
  },
  plugins: [
    /*
     * HtmlWebPackPlugin will generate an HTML file that includes all webpack bundles in the
     * body using script tags.
     */
    new HtmlWebPackPlugin({
      template: "./src/index.html"
    })
  ]
};
