import assert from "assert";
import Bar from "../bar";

describe("Bar", () => {
  it("The greet function should pass", () => {
    let bar = new Bar();
    assert.equal(bar.greet("Foo"), "Hello, Foo!");
  });
});
