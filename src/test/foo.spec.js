import assert from "assert";
import Foo from "../foo";

describe("Foo", () => {
  it("The sum function should pass", () => {
    let foo = new Foo();
    assert.equal(foo.sum(1, 1), 2);
  });
  it("The subtract function should pass", () => {
    let foo = new Foo();
    assert.equal(foo.subtract(1, 1), 0);
  });
});
