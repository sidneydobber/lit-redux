import { all } from 'redux-saga/effects';
import fooSaga from '../app-saga';

function* rootSaga() {
    yield all([
        fooSaga(),
    ]);
}

export default rootSaga;
