import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

// Reducers
import createReducer from './reducers.js';

// Sagas
import rootSaga from './sagas';

// Middlewares
const sagaMiddleware = createSagaMiddleware();

function configureStore(initialState = {}) {
  const middlewares =[sagaMiddleware];
  const enhancers = [applyMiddleware(...middlewares)];

  const composeEnhancers =
    process.env.NODE_ENV !== 'production' && typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
      : compose;

  return createStore(createReducer(), initialState, composeEnhancers(...enhancers));
}

const store = configureStore();

// Extensions
store.runSaga = sagaMiddleware.run(rootSaga);

export default store;
