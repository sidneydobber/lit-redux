import { combineReducers } from 'redux';
import reducer from '../app-reducer';

export default function createReducer() {
  return combineReducers({
    foo: reducer,
    bar: () => {
      return {}
    }
  });
}
