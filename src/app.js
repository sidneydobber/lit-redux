import { LitElement, html } from "lit-element";
import { connect } from "pwa-helpers";
import { compose } from "ramda";
import store from "./setup/store";
import { foo } from "./app-actions";

const Mixins = compose(connect(store))(LitElement);

class App extends Mixins {
  static get properties() {
    return {
      text: { type: String }
    };
  }

  constructor() {
    super();
    store.dispatch(foo({ text: "Hello, worlds!" }));
  }

  connectedCallback() {
    super.connectedCallback();
  }

  stateChanged(state) {
    this.text = state.foo.text;
  }

  render() {
    return html`
      <h1>${this.text}</h1>
    `;
  }
}

customElements.define("redux-app", App);
