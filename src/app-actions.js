export const FOO_REQUEST = 'APP@@FOO_REQUEST';
export const FOO_SUCCESS = 'APP@@FOO_SUCCESS';

export const foo = (payload) => ({
    type: FOO_REQUEST,
    payload
});
