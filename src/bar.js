export default class Bar {
  greet(name) {
    return `Hello, ${name}!`;
  }
  goodbye(name) {
    return `See you later, ${name}!`;
  }
}
