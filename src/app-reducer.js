import * as actions from "./app-actions";

export const initialState = {};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actions.FOO_SUCCESS: {
      console.log("world!");
      return Object.assign({}, { text: payload.text });
    }
    default:
      return state;
  }
};

export default reducer;
