import { all, takeEvery, put, call, select } from "redux-saga/effects";
import { FOO_REQUEST, FOO_SUCCESS } from "./app-actions";

function* handleFooSaga(action) {
  const textUpperCase = action.payload.text.toUpperCase();
  console.log("Hello, ");
  yield put({
    type: FOO_SUCCESS,
    payload: {
      text: textUpperCase
    }
  });
}

function* appSaga() {
  yield takeEvery(FOO_REQUEST, handleFooSaga);
}

export default function*() {
  yield all([appSaga()]);
}
